# PassionView

Get view logic out of persistence models and controllers and put it into view models.

## How does it work?

Let say you need to display some information on a page related to a model.
You also need to add some logic unrelated to the model itself.

Of course your extra logic can't be in the controller because you're not barbaric.
It also can't be in the model because it's just related to this action and honestly you already have too many things in there and that is one of the reasons you're here.

Your model is `OldSock (:size, :color, :stink)`

**In the controller:**
```ruby
def show
 old_sock = OldSock.find(id)
 @old_sock_view_model = OldSockViewModel.new(old_sock)
end
```

**The view model would look like:**
```ruby
class OldSockViewModel < PassionView::Card::Base
 include PassionView::Resourceful # this mean you can use @resource (your model)

 delegate :size,
          :color,
          :stink,
          to: @resource # There you have your model old_sock

 def extra_logic
   #...your super code...
 end
end
```

**In the view:**
```ruby
 ...
 @old_sock_view_model.size
 @old_sock_view_model.color
 @old_sock_view_model.extra_logic

 @old_sock_view_model.age # This one doesn't work of course
 ...
```

You can also use a view_model without resource and without model. My example is just clearer this way

## What about forms

Of course some of you might want to add some new old socks too.

**In the controller:**
```ruby
def create
 @old_sock_form = OldSockForm.new(OldSock.new)
 @old_sock_form.update_attributes(params.require(:old_sock))

 # You might also consider
 @old_sock_form.contextualize_routes(self) # if you want to add some path
 @old_sock_form.contextualize_auth(user) # If the user might intervene in your logic
end

```

**The form would look like:**

```ruby
class OldSockForm < PassionView::Form::Base
 include PassionView::Routable # because of the path
 include PassionView::Authorization # because of the user

 delegate :size,
          :color,
          to: @resource

 delegate :stink,
          to: @resource,
          cast: integer

 validates :size, presence: true # Yep that right that where you put your validation

 def path # I don't know you maybe you'll need this too. It's your socks you do what you want.
   routes.old_socks_path
 end
end
```

But wait what about strong parameters. Well the delegation is taking care of it for you.

You can also cast your input in integer, or dates.


## What about Index

We call them **collections**.

```ruby
class OldSockCollection < PassionView::Card::Base
 include PassionView::Card::Collection
 include PassionView::Paginable # Pagination of course

 def item_view_model_class
   OldSockViewModel
 end
end
```

And that's it you have a paginable list of OldSockViewModel.

## What else?

**There is also:**
* cards which is basically a view_model with actions (or paths)
* filters
* auto-delegation

and... no I think that's it

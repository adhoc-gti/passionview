Gem::Specification.new do |s|
  s.name        = 'passionview'
  s.version     = '0.2.4'
  s.licenses    = ['BSD-3-Clause']
  s.summary     = 'Love your views'
  s.description = <<-DESC
    Get view logic out of persistence models and controllers and put it into
    view models.
  DESC
  s.authors     = ['ADHOC-GTI']
  s.email       = 'foss@adhoc-gti.com'
  s.files       = Dir['lib/**/*.rb'] + ['LICENSE']
  s.homepage    = 'https://gitlab.com/adhoc-gti/passionview'
  s.add_development_dependency 'rubocop', '~> 0.46.0'
end

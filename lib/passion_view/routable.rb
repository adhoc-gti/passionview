# Routable exposes contextualized routes in an object
module PassionView::Routable
  extend ActiveSupport::Concern
  attr_reader :controller

  def contextualize_routes(controller)
    @controller = controller
    routes.contextualize(controller)
  end

  def routes
    @_routes_context ||= RouteContext.new
  end

  class RouteContext
    def default_url_options
      @_context ? @_context.default_url_options : {}
    end

    def _routes_context
      self
    end

    def _with_routes(routes)
      old_routes, @_routes = @_routes, routes
      yield
    ensure
      @_routes = old_routes
    end

    def contextualized?
      !@_context.nil?
    end

    def contextualize(obj)
      @_context = obj
    end

    def without_context
      self.class.new
    end
  end
end

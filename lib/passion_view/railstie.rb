class PassionView::MyRailtie < Rails::Railtie
  config.to_prepare do
    PassionView::Routable::RouteContext.instance_eval do
      include Rails.application.routes.url_helpers
      include Rails.application.routes.mounted_helpers
    end
  end
end

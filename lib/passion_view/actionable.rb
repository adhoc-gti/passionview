module PassionView::Actionable
  extend ActiveSupport::Concern

  # well : menu en bas de page
  # menu : menu sur l'entite
  # nav : menu de navigation
  # item : menu dans une liste
  CONTEXTS = [:well, :menu, :nav, :item].freeze
  TYPES = [:nav, :action].freeze

  class Action
    KEYS = [
      :name, :icon, :path,
      :type, :context, :priority,
      :options
    ].freeze

    attr_accessor(*KEYS)

    def initialize(attributes)
      attributes.each do |key, value|
        raise unless respond_to?(key)
        send("#{key}=", value)
      end
    end

    def [](key)
      return unless KEYS.include?(key)

      send(key)
    end
  end

  class ActionList < Array
    def context(context)
      select do |a|
        case a.context
        when Symbol
          a.context == context
        when Array
          a.context.include?(context)
        end
      end
    end

    def type(type)
      select do |a|
        case a.type
        when Symbol
          a.type == type
        when Array
          a.type.include?(type)
        end
      end
    end
  end

  def actions
    return @actions unless @actions.nil?
    @actions = ActionList.new

    self.class.action_blocks.each do |block|
      options = instance_eval(&block)
      next if options.nil?

      @actions << Action.new(options)
    end

    @actions
  end

  module ClassMethods
    def action_blocks
      @action_blocks || []
    end

    def add_action(&block)
      @action_blocks ||= []
      @action_blocks << block
    end
  end
end

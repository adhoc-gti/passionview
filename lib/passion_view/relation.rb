module PassionView::Relation
  # delegate :to_sql,
  #          :offset_value,
  #          :total_count,
  #          :limit_value,
  #          to: :scoped

  def items
    case (items = super)
    when ActiveRecord::Relation
      items
    when ActiveRecord::Base
      items.where(nil)
    else
      raise TypeError, "not an ActiveRecord object: #{items.class}"
    end
  end

  def first
    item_view_model(items.first)
  end

  def last
    item_view_model(items.first)
  end

  delegate :count, to: :items

  delegate :size, to: :items

  delegate :include?, to: :items

  delegate :empty?, to: :items

  delegate :to_sql, to: :items

  delegate :offset_value, to: :items

  delegate :total_count, to: :items

  delegate :limit_value, to: :items
end

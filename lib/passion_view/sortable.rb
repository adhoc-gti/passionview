module PassionView::Sortable
  extend ActiveSupport::Concern

  include PassionView::Pathable

  attr_accessor :sort_column, :sort_direction

  def initialize(items, options = {})
    super
    sort = options.delete(:sort) || {}

    sort_with(sort[:sort], sort[:direction])
  end

  def items
    super.order(order_arel[sort_column].send(sort_direction))
  end

  def sort_columns_available
    order_arel.keys
  end

  def sort_column_available?(name)
    sort_columns_available.include?(name.to_s)
  end

  def sort_directions_available
    %w(asc desc)
  end

  def default_sort_column
    resource_class.primary_key
  end

  def default_sort_direction
    sort_directions_available.first
  end

  def sort_by_path(column, direction = default_sort_direction)
    sortable_path(sortable_params(column, direction))
  end

  def sortable_path(options)
    resource_path(url_options.merge(options))
  end

  def url_options
    super.merge(sortable_params)
  end

  def order_arel
    table = resource_class.arel_table
    pk = default_sort_column

    { pk => table[pk.to_sym] }
  end

  def sortable_params(column = nil, direction = nil)
    params = {}

    column    ||= sort_column
    direction ||= sort_direction

    params[:sort]      = column
    params[:direction] = direction

    params
  end

  def sort_with(column, direction)
    self.sort_column = column || default_sort_column
    self.sort_column = default_sort_column unless sort_columns_available.include?(sort_column)

    self.sort_direction = direction || default_sort_direction
    self.sort_direction = default_sort_direction unless %w(asc desc).include?(sort_direction)
  end

  module Controller
    extend ActiveSupport::Concern

    def sort_params
      params.permit(:sort, :direction).to_h
    end
  end
end

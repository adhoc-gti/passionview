# items: enumerable
module PassionView::ViewModel::List
  extend ActiveSupport::Concern
  extend Enumerable

  def initialize(items, options = {})
    super
    @items = items
    @item_view_model_class = options[:item_view_model_class]
  end

  def each
    return enum_for(:each) unless block_given?

    items.each do |e|
      yield item_view_model(e)
    end
  end

  def item_view_model(item)
    card = item_view_model_class.new(item)

    card.contextualize_auth(user)
    card.contextualize_routes(@controller) if @controller
    card
  end

  delegate :records,
           to: :items

  delegate :to_a,
           to: :items

  private

  attr_reader :items
  attr_reader :item_view_model_class
end

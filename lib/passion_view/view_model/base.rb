class PassionView::ViewModel::Base
  extend ActiveModel::Naming
  extend ActiveModel::Translation

  def initialize(_, options = {})
    @options = options
  end

  def self.i18n_scope
    :cards
  end

  def self.lookup_ancestors
    [self]
  end

  private

  attr_reader :options
end

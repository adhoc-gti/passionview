module PassionView::Paginable
  extend ActiveSupport::Concern

  include PassionView::Pathable

  attr_accessor :current_page, :current_per

  DEFAULT_PER_PAGE = 10

  def initialize(items, options = {})
    super
    pagination = options.delete(:pagination) || {}

    paginate_with(pagination[:page], pagination[:per])
  end

  def items
    super.page(current_page)
         .per(current_per)
  end

  def next_pages(count = 2)
    (-count..count).each do |idx|
      target = current_page + idx
      yield target, next_page_path(idx) if block_given? && !out_of_range?(target)
    end
  end

  def first_page?(page = nil)
    page ||= current_page
    page.to_i == first_page
  end

  def last_page?(page = nil)
    page ||= current_page
    page.to_i == last_page
  end

  def current_page?(page)
    page.to_i == current_page
  end

  def first_page
    1
  end

  def previous_page
    next_page(-1)
  end

  def next_page(shift = 1)
    target = current_page + shift
    target = first_page if target < first_page
    target = last_page  if target > last_page

    target
  end

  def last_page
    [items.total_pages, 1].max
  end

  def out_of_range?(target)
    (target < first_page) || (target > last_page)
  end

  def first_page_path
    paginable_path(page: first_page)
  end

  def previous_page_path
    paginable_path(page: previous_page)
  end

  def next_page_path(shift = 1)
    paginable_path(page: next_page(shift))
  end

  def last_page_path
    paginable_path(page: last_page)
  end

  def paginable_path(options)
    page_path(url_options.merge(options))
  end

  def page_path(options = {})
    options
  end

  def url_options
    super.merge(paginable_params)
  end

  def paginable_params(page = nil, per = nil)
    params = {}
    page ||= current_page
    per  ||= current_per

    params[:page] = page unless page == first_page
    params[:per]  = per  unless per.nil? || per == DEFAULT_PER_PAGE

    params
  end

  def paginate_with(page, per = nil)
    self.current_page = page.nil? ? 1 : page.to_i
    self.current_per  = per.nil?  ? DEFAULT_PER_PAGE : per.to_i
  end

  module Controller
    extend ActiveSupport::Concern

    def pagination_params
      params.permit(:page, :per).to_h
    end
  end
end

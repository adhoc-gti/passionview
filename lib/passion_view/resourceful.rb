# Resourceful makes an instance a resource in terms of Rails routing
module PassionView::Resourceful
  extend ActiveSupport::Concern

  include PassionView::Routable

  def initialize(resource, options = {})
    super
    @resource = resource
    @parent_resource = options[:parent]
  end

  def resource
    @resource
  end

  def resource_class
    @resource.klass #### @objects
  end
end

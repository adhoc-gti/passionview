module PassionView::Filterable
  extend ActiveSupport::Concern

  def initialize(items, options = {})
    super
    filters = filters_default.merge(options.delete(:filters) || {})

    filter_with(filters)
  end

  def items
    filters.reduce(super) do |items, filter|
      filter.skip? ? items : filter.apply(items)
    end
  end

  def filter_path
    url_options
  end

  attr_reader :filters

  private

  def filters_default
    {}
  end

  def url_options
    super.merge(filterable_params)
  end

  def filterable_params
    filters.each.with_object(filters: {}) { |filter, params| params[:filters][filter.name.to_sym] = filter.value unless filter.skip? }
  end

  def filter_with(_)
    @filters ||= FilterList.new
  end

  module Controller
    extend ActiveSupport::Concern

    def filter_params
      params[:filters]&.to_unsafe_h || {}
    end

    module ClassMethods
      def filter(*actions)
        filtered_actions.push(*actions)
      end
    end
  end

  class FilterList < Array
    def present
      reject(&:skip?)
    end
  end
end

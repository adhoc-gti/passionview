# Authorization manages context necessary for auth
module PassionView::Authorization
  attr_accessor :user

  def contextualize_auth(user)
    self.user = user
  end
end

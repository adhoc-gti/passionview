module PassionView::Delegatable
  extend ActiveSupport::Concern

  module ClassMethods
    def delegate_prefix(*method_names)
      @prefixes ||= {}
      @prefixes["#{method_names.join('_')}_"] = method_names
    end

    def prefixes
      @prefixes || {}
    end
  end

  def method_missing(m, *args, &block)
    self.class.prefixes.each do |prefix, method_names|
      next unless m.to_s.start_with?(prefix)

      last_m = m.to_s.gsub(prefix, '').to_sym
      return method_names.inject(resource) do |result, method|
        break if result.nil?

        result.send method
      end&.send(last_m)
    end

    return resource.send(m, *args, &block) if resource.respond_to?(m)

    super
  end

  def respond_to_missing?(m, _)
    resource.respond_to?(m)
  end
end

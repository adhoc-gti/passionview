class PassionView::Card::Base < PassionView::ViewModel::Base
  include PassionView::Routable
  include PassionView::Actionable
  include PassionView::Resourceful
  include PassionView::Authorization

  def to_key
    [resource.id]
  end

  def to_model
    resource
  end

  delegate :to_param, to: :resource

  def self.i18n_scope
    :cards
  end
end

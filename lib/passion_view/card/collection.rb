module PassionView::Card::Collection
  extend ActiveSupport::Concern

  include PassionView::ViewModel::List

  def initialize(objects, options = {})
    super(objects, options)
  end
end

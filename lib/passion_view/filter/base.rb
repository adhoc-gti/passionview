class PassionView::Filter::Base < PassionView::ViewModel::Base
  def initialize(name, options = {}, &block)
    super(name, options)
    @name = name
    @value = options[:value]
    @block = block
  end

  def skip?
    value.blank?
  end

  attr_reader :name, :value

  def apply(items)
    return @block.call(items, value) if @block
  end
end

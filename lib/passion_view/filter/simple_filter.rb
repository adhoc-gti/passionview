class PassionView::Filter::SimpleFilter < PassionView::Filter::Base
  def initialize(name, field:, value: [], &block)
    super
    @field = field
  end

  def apply(items)
    super

    items.where(field => value)
  end

  private

  attr_reader :field
end
